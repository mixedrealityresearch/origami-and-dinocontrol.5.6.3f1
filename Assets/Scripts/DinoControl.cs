﻿using System.Collections;
using System.Collections.Generic;
using Academy.HoloToolkit.Unity;
using UnityEngine;

public class DinoControl : MonoBehaviour {

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        if (GestureManager.Instance.isDinosaurMove)
        {
            Debug.Log("You should move the dinosaur. isDinoShouldMove: " + GestureManager.Instance.isDinosaurMove);

            //gameObject.transform.position = GameObject.Find("RedCube").transform.position;

            // check the current gazed object
            if (GazeGestureManager.Instance.FocusedObject != null)
            {
                Debug.Log("You gaze at: " + GazeGestureManager.Instance.FocusedObject.name);
                gameObject.transform.position = GazeGestureManager.Instance.FocusedObject.transform.position;
            }
            else
            {
                Debug.Log("FocusedObject is NULL");
            }
            
        }
	}
}
