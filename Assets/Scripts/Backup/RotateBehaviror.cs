﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateBehaviror : MonoBehaviour
{
    float speed = 0.003f;
    float temp = 100.0f;

    void Update()
    {
        var x = transform.position.x;
        var y = transform.position.y;

        x -= speed;
        transform.position = new Vector3(x, transform.position.y, transform.position.z);
        transform.Rotate(Vector3.up, temp * Time.deltaTime);
    }
}