﻿using UnityEngine;
using System;

public class CubeCommands : MonoBehaviour
{
    // Called by GazeGestureManager when the user performs a Select gesture
    void OnSelect()
    {
        // If the sphere has no Rigidbody component, add one to enable physics.
        if (!this.GetComponent<Rigidbody>())
        {
            var rigidbody = this.gameObject.AddComponent<Rigidbody>();
            rigidbody.collisionDetectionMode = CollisionDetectionMode.Continuous;
        }
    }

    //void OnStart()
    //{
    //    CallOnParent(r => r.isStopped = false);
    //}

    //void OnStop()
    //{
    //    CallOnParent(r => r.isStopped = true);
    //}

    //void OnQuick()
    //{
    //    CallOnParent(r => r.isFast = true);
    //}

    //void OnSlow()
    //{
    //    CallOnParent(r => r.isFast = false);
    //}

    //void CallOnParent(Action<RotateBehaviror> f)
    //{
    //    //var rot = this.gameObject.GetComponentInParent<Rotate>();
    //    var rot = this.gameObject.GetComponent<RotateBehaviror>();
    //    if (rot)
    //    {
    //        f(rot);
    //    }
    //}

    // Called by GazeGestureManager when the user performs a Select gesture

    void GoRight()
    {
        this.gameObject.GetComponent<RotateBehaviror>();
    }
}